package com.bookstore.bookstore.model;

import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "publisher")
@Access(value=AccessType.FIELD)
public class Publisher {
	
	@OneToMany(mappedBy = "publisher", cascade = CascadeType.ALL)
	private Set<Book> pBooks;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long id;

	private String companyName;
	
	private String pubCountry;
	
	@ManyToOne
	@JoinColumn
	private Paper paper;
	
	
	//Setter Getters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPubCountry() {
		return pubCountry;
	}

	public void setPubCountry(String pubCountry) {
		this.pubCountry = pubCountry;
	}

	public Paper getPaper() {
		return paper;
	}

	public void setPaper(Paper paper) {
		this.paper = paper;
	}



	
	
	
	
//	PUBLISHER
//	*Publisher ID
//	* Company Name
//	* Country
//	* Paper
}
