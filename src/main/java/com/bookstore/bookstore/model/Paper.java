package com.bookstore.bookstore.model;

import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "papers")
@Access(value=AccessType.FIELD)
@JsonIgnoreProperties({"paper"})
public class Paper {
	
	@OneToMany(mappedBy = "paper", cascade = CascadeType.ALL)
	private Set<Publisher> publisher;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String qualityName;
	
	private double paperPrice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public double getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(double paperPrice) {
		this.paperPrice = paperPrice;
	}

	
	
//	Paper Quality
//	*Paper ID
//	*QualityName
//	*PaperPrice (edited) 
}
