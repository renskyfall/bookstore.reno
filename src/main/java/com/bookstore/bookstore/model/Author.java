package com.bookstore.bookstore.model;

import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "author")
@Access(value=AccessType.FIELD)
public class Author {
	
	@OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
	private Set<Book> aBooks;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String firstName;
	
	private String lastName;
	
	private String gender;
		
	private int age;
	
	private String country;
	
	private String rating;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String fName) {
		this.firstName = fName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lName) {
		this.lastName = lName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}
	
//	@Override
//	public String toString() {
//		String read;
//		read = "id : "+id;
//		read += "\nf_name : "+fName;
//		read += "\nl_name : "+lName;
//		read += "\ngender : "+gender;
//		read += "\nage :"+age;
//		return read;
//	}
	
//- Author
//	*Author ID
//	* First Name
//	* Last Name
//	* Gender
//	* Age
//	* Country
//	* Rating
}
