package com.bookstore.bookstore.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstore.model.Book;
import com.bookstore.bookstore.repositories.BookRepository;


@RestController
@RequestMapping("/query")
public class BookQuery {
	
	@Autowired
	BookRepository bookRepository;
	
	//Menampilkan semua Employee
	@GetMapping("/books")
    public List<Book> showList() {
        return bookRepository.findAllActiveUsers();
    }
	
	//Mencari Employee dengan ID
	@GetMapping("/books/{id}")
	public HashMap<String, Object> showByID(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> process = new HashMap<String, Object>();
		Book employee = bookRepository.searchByID(id);
		
		process.put("Messages", "Search By ID Success");
		process.put("Data", employee);
		return process;
    }

	//Menambahkan Employee
	@PostMapping("/books/add")
    public HashMap<String, Object> add(@Valid @RequestBody Book book) {
        HashMap<String, Object> addProcess = new HashMap<String, Object>();
        
        bookRepository.addBook(book.getPrice(), book.getReleaseDate(), book.getTitle(), book.getAuthor(), book.getPublisher());
		
		addProcess.put("Message", "Success");
		addProcess.put("Now Data", bookRepository.findAll());
		return addProcess;
    }
	
	//Mengedit Notes
	@PutMapping("/books/{id}")
    public HashMap<String, Object> updateBook(@PathVariable(value = "id") Long bookID, @Valid @RequestBody Book book) {
		HashMap<String, Object> process = new HashMap<String, Object>();
		Book tempBook = bookRepository.searchByID(bookID);        
		
        book.setId(tempBook.getId());
        if (book.getPrice() == 0) {
        	book.setPrice(tempBook.getPrice());
        }
        if (book.getReleaseDate() == null) {
        	book.setReleaseDate(tempBook.getReleaseDate());
        }
        if (book.getTitle() == null) {
        	book.setTitle(tempBook.getTitle());
        }
        if (book.getAuthor() == null) {
        	book.setAuthor(tempBook.getAuthor());
        }
        if (book.getPublisher() == null) {
        	book.setPublisher(tempBook.getPublisher());
        }
        
        bookRepository.updateBook(book.getPrice(), book.getReleaseDate(), book.getTitle(), book.getAuthor(), book.getPublisher(), book.getId());
        process.put("Message", "Success Updated");
        process.put("Data", book);
        return process;
    }
	
	//Menghapus Employee		
	@DeleteMapping("/books/{id}")
    public HashMap<String, Object> deleteBook(@PathVariable(value = "id") Long bookID) {
		HashMap<String, Object> process = new HashMap<String, Object>();
		Book book = bookRepository.searchByID(bookID);
		
		process.put("Messages", "Success Deleting Data Book");
		process.put("Delete data :", book);
		bookRepository.deleteBook(bookID);
        return process;
    }
}
