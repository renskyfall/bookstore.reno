package com.bookstore.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstore.model.Paper;
import com.bookstore.bookstore.repositories.PaperRepository;

@RestController
@RequestMapping("/api")
public class PaperController {

    @Autowired
    PaperRepository paperRepository;
    
    // Get All Notes
    @GetMapping("/papers")
    public HashMap<String, Object> getAll() {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	process.put("Messages", "Read All Data Success");
    	process.put("Total Data", paperRepository.findAll().size());
    	process.put("Data", paperRepository.findAll());
    	return process;
    }  
    
    // Create a new Note
    @PostMapping("/papers/create")
    public HashMap<String, Object> create(@Valid @RequestBody Paper paperBody) {
    	HashMap<String, Object> process = new HashMap<String, Object>();    	
    	Paper paper = paperRepository.save(paperBody);
        process.put("Messages", "Create Paper Success");
    	process.put("Data", paper);
    	return process;
    }
    
    // Create more
    @PostMapping("/papers/createMore")
    public HashMap<String, Object> createMore(@Valid @RequestBody Paper... paper) {
    	HashMap<String, Object> newList = new HashMap<String, Object>();
    	List<Paper> list = new ArrayList<Paper>();
    	int total = 0;
        for(Paper test : paper) {
    		total++;
    		list.add(paperRepository.save(test));
    	}
        newList.put("Message", "Create More Papers Success");
        newList.put("Total", total);
    	newList.put("Data", list);
    	return newList;
    }
   
    // Get a Single Note
    @GetMapping("/papers/{id}")
    public HashMap<String, Object> getById(@PathVariable(value = "id") Long paperId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();	
        Paper paper = paperRepository.findById(paperId).orElse(null);
        process.put("Messages", "Read Data Success");
    	process.put("Data", paper);
        return process;
    }
    
    // Update a Note
    @PutMapping("/papers/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long paperId, @Valid @RequestBody Paper paper) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Paper tempPaper = paperRepository.findById(paperId).orElse(null);      
 
        if (paper.getQualityName() != null) {
        	tempPaper.setQualityName(paper.getQualityName());
        }
        if (paper.getPaperPrice() != 0) {
        	tempPaper.setPaperPrice(paper.getPaperPrice());
        }
        
        paperRepository.save(tempPaper);
        process.put("Message", "Success Updated Data");
        process.put("Data", paper);
        return process;
    }
    
    // Delete a Note
    @DeleteMapping("/papers/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long paperID) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
        Paper paper = paperRepository.findById(paperID).orElse(null);

        paperRepository.delete(paper);

        process.put("Messages", "Success Deleting Data Paper");
		process.put("Delete data :", paper);
        return process;
    }
   
}