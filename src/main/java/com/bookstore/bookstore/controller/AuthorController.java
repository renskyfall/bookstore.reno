package com.bookstore.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstore.model.Author;
import com.bookstore.bookstore.repositories.AuthorRepository;

@RestController
@RequestMapping("/api")
public class AuthorController {

    @Autowired
    AuthorRepository authorRepository;
    
    // Get All Notes
    @GetMapping("/authors")
    public HashMap<String, Object> getAll() {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	process.put("Messages", "Read All Data Success");
    	process.put("Total Data", authorRepository.findAll().size());
    	process.put("Data", authorRepository.findAll());
    	return process;
    }  
    
    // Create a new Note
    @PostMapping("/authors/create")
    public HashMap<String, Object> create(@Valid @RequestBody Author authorBody) {
    	HashMap<String, Object> process = new HashMap<String, Object>();    	
    	Author author = authorRepository.save(authorBody);
        process.put("Messages", "Create Author Success");
    	process.put("Data", author);
    	return process;
    }
    
    // Create more
    @PostMapping("/authors/createMore")
    public HashMap<String, Object> createMore(@Valid @RequestBody Author... author) {
    	HashMap<String, Object> newList = new HashMap<String, Object>();
    	List<Author> list = new ArrayList<Author>();
    	int total = 0;
        for(Author test : author) {
    		total++;
    		list.add(authorRepository.save(test));
    	}
        newList.put("Message", "Create More Authors Success");
        newList.put("Total", total);
    	newList.put("Data", list);
    	return newList;
    }
   
    // Get a Single Note
    @GetMapping("/authors/{id}")
    public HashMap<String, Object> getById(@PathVariable(value = "id") Long authorId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();	
        Author author = authorRepository.findById(authorId).orElse(null);
        process.put("Messages", "Read Data Success");
    	process.put("Data", author);
        return process;
    }
    
    // Update a Note
    @PutMapping("/authors/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long authorId, @Valid @RequestBody Author author) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Author tempAuthor = authorRepository.findById(authorId).orElse(null);      

        if (author.getFirstName() != null) {
        	tempAuthor.setFirstName(author.getFirstName());
        }
        if (author.getLastName() != null) {
        	tempAuthor.setLastName(author.getLastName());
        }
        if (author.getGender() != null) {
        	tempAuthor.setGender(author.getGender());
        }
        if (author.getAge() != 0) {
        	tempAuthor.setAge(author.getAge());
        }
        if (author.getCountry() != null) {
        	tempAuthor.setCountry(author.getCountry());
        }
        if (author.getRating() != null) {
        	tempAuthor.setRating(author.getRating());
        }
        
        authorRepository.save(tempAuthor);
        process.put("Message", "Success Updated Data");
        process.put("Data", author);
        return process;
    }
    
    // Delete a Note
    @DeleteMapping("/authors/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long authorID) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
        Author author = authorRepository.findById(authorID).orElse(null);

        authorRepository.delete(author);

        process.put("Messages", "Success Deleting Data Author");
		process.put("Delete data :", author);
        return process;
    }
   
}