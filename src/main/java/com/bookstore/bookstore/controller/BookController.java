package com.bookstore.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstore.model.Book;
import com.bookstore.bookstore.repositories.BookRepository;

@RestController
@RequestMapping("/api")
public class BookController {

    @Autowired
    BookRepository bookRepository;
    
    // Get All Notes
    @GetMapping("/books")
    public HashMap<String, Object> getAll() {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	process.put("Messages", "Read All Data Success");
    	process.put("Total Data", bookRepository.findAll().size());
    	process.put("Data", bookRepository.findAll());
    	return process;
    }  
    
    // Create a new Note
    @PostMapping("/books/create")
    public HashMap<String, Object> create(@Valid @RequestBody Book note) {
    	HashMap<String, Object> process = new HashMap<String, Object>();    	
    	Book book = bookRepository.save(note);
        process.put("Messages", "Create Book Success");
    	process.put("Data", book);
    	return process;
    }
    
    // Create more
    @PostMapping("/books/createMore")
    public HashMap<String, Object> createMore(@Valid @RequestBody Book... book) {
    	HashMap<String, Object> newList = new HashMap<String, Object>();
    	List<Book> list = new ArrayList<Book>();
    	int total = 0;
        for(Book test : book) {
    		total++;
    		list.add(bookRepository.save(test));
    	}
        newList.put("Message", "Create More Books Success");
        newList.put("Total", total);
    	newList.put("Data", list);
    	return newList;
    }
   
    // Get a Single Note
    @GetMapping("/books/{id}")
    public HashMap<String, Object> getById(@PathVariable(value = "id") Long bookId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();	
        Book book = bookRepository.findById(bookId).orElse(null);
        process.put("Messages", "Read Data Success");
    	process.put("Data", book);
        return process;
    }
    
    // Update a Note
    @PutMapping("/books/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long bookId, @Valid @RequestBody Book book) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Book tempBook = bookRepository.findById(bookId).orElse(null);      
		
        if (book.getPrice() != 0) {
        	tempBook.setPrice(book.getPrice());
        }
        if (book.getReleaseDate() != null) {
        	tempBook.setReleaseDate(book.getReleaseDate());
        }
        if (book.getTitle() != null) {
        	tempBook.setTitle(book.getTitle());
        }
        if (book.getAuthor() != null) {
        	tempBook.setAuthor(book.getAuthor());
        }
        if (book.getPublisher() != null) {
        	tempBook.setPublisher(book.getPublisher());
        }
        
        bookRepository.save(tempBook);
        process.put("Message", "Success Updated Data");
        process.put("Data", book);
        return process;
    }
    
    // Delete a Note
    @DeleteMapping("/books/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long bookID) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
        Book book = bookRepository.findById(bookID).orElse(null);

        bookRepository.delete(book);

        process.put("Messages", "Success Deleting Data Book");
		process.put("Delete data :", book);
        return process;
    }
   
}