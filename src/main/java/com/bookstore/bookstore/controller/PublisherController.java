package com.bookstore.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstore.model.Publisher;
import com.bookstore.bookstore.repositories.PublisherRepository;

@RestController
@RequestMapping("/api")
public class PublisherController {

    @Autowired
    PublisherRepository publisherRepository;
    
    // Get All Notes
    @GetMapping("/publishers")
    public HashMap<String, Object> getAll() {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	process.put("Messages", "Read All Data Success");
    	process.put("Total Data", publisherRepository.findAll().size());
    	process.put("Data", publisherRepository.findAll());
    	return process;
    }  
    
    // Create a new Note
    @PostMapping("/publishers/create")
    public HashMap<String, Object> create(@Valid @RequestBody Publisher publisherBody) {
    	HashMap<String, Object> process = new HashMap<String, Object>();    	
    	Publisher publisher = publisherRepository.save(publisherBody);
        process.put("Messages", "Create Publisher Success");
    	process.put("Data", publisher);
    	return process;
    }
    
    // Create more
    @PostMapping("/publishers/createMore")
    public HashMap<String, Object> createMore(@Valid @RequestBody Publisher... publisher) {
    	HashMap<String, Object> newList = new HashMap<String, Object>();
    	List<Publisher> list = new ArrayList<Publisher>();
    	int total = 0;
        for(Publisher test : publisher) {
    		total++;  		
    		list.add(publisherRepository.save(test));
    	}
        newList.put("Message", "Create More Publishers Success");
        newList.put("Total", total);
    	newList.put("Data", list);
    	return newList;
    }
   
    // Get a Single Note
    @GetMapping("/publishers/{id}")
    public HashMap<String, Object> getById(@PathVariable(value = "id") Long publisherId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();	
        Publisher publisher = publisherRepository.findById(publisherId).orElse(null);
        process.put("Messages", "Read Data Success");
    	process.put("Data", publisher);
        return process;
    }
    
    // Update a Note
    @PutMapping("/publishers/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long paperId, @Valid @RequestBody Publisher publisher) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Publisher tempPublisher = publisherRepository.findById(paperId).orElse(null);      
    	
        if (publisher.getCompanyName() != null) {
        	tempPublisher.setCompanyName(publisher.getCompanyName());
        }
        if (publisher.getPubCountry() != null) {
        	tempPublisher.setPubCountry(publisher.getPubCountry());
        }
        if (publisher.getPaper() != null) {
        	tempPublisher.setPaper(publisher.getPaper());
        }
        
        publisherRepository.save(tempPublisher);
        process.put("Message", "Success Updated Data");
        process.put("Data", publisher);
        return process;
    }
    
    // Delete a Note
    @DeleteMapping("/publishers/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long publisherID) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
        Publisher publisher = publisherRepository.findById(publisherID).orElse(null);

        publisherRepository.delete(publisher);

        process.put("Messages", "Success Deleting Data Publisher");
		process.put("Delete data :", publisher);
        return process;
    }
   
}