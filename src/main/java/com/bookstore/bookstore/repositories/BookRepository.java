package com.bookstore.bookstore.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.bookstore.model.Author;
import com.bookstore.bookstore.model.Book;
import com.bookstore.bookstore.model.Publisher;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
	
	@Query(value = "select * from Book", nativeQuery = true)
	public List<Book> findAllActiveUsers();
	
	@Query(value = "select * from Book where id = :id", nativeQuery = true)
	public Book searchByID(@Param("id") Long id);
	
	@Modifying
	@Query(value = "INSERT INTO Book (price, rel_date, title, author_id, publisher_id) VALUES (:price, :rel_date, :title, :author, :publisher)", nativeQuery = true)
	@Transactional
	public void addBook(@Param("price") double price, @Param("rel_date") Date relDate, @Param("title") String title, @Param("author") Author author, @Param("publisher") Publisher publisher);
	
	@Modifying
	@Query(value = "UPDATE Book SET price = :price, rel_date = :rel_date, title = :title, author_id = :author, publisher_id = :publisher WHERE id = :id", nativeQuery = true)
	@Transactional
	public void updateBook(@Param("price") double price, @Param("rel_date") Date relDate, @Param("title") String title, @Param("author") Author author, @Param("publisher") Publisher publisher, @Param("id") Long id);
	
	@Modifying
	@Query(value = "DELETE FROM Book WHERE id = :id", nativeQuery = true)
	@Transactional
	public void deleteBook(@Param("id") Long id);
}