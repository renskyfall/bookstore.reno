package com.bookstore.bookstore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.bookstore.model.Paper;

@Repository
public interface PaperRepository extends JpaRepository<Paper, Long> {
	
	@Query(value = "select * from Papers", nativeQuery = true)
	public List<Paper> findAllActiveUsers();
	
	@Query(value = "select * from Papers where id = :id", nativeQuery = true)
	public Paper searchByID(@Param("id") Long id);
	
	@Modifying
	@Query(value = "INSERT INTO Papers (quality_name, paper_price) VALUES (:quality_name, :paper_price)", nativeQuery = true)
	@Transactional
	public void addPaper(@Param("quality_name") String fName, @Param("paper_price") double price);
	
	@Modifying
	@Query(value = "UPDATE Papers SET quality_name = :quality_name, paper_price = :paper_price WHERE id = :id", nativeQuery = true)
	@Transactional
	public void updatePaper(@Param("quality_name") String fName, @Param("paper_price") double price, @Param("id") Long id);
	
	@Modifying
	@Query(value = "DELETE FROM Papers WHERE id = :id", nativeQuery = true)
	@Transactional
	public void deletePaper(@Param("id") Long id);
	
//	Paper Quality
//	*Paper ID
//	*QualityName
//	*PaperPrice (edited) 
}