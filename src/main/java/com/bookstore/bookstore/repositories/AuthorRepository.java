package com.bookstore.bookstore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.bookstore.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
	
	@Query(value = "select * from Author", nativeQuery = true)
	public List<Author> findAllActiveUsers();
	
	@Query(value = "select * from Author where id = :id", nativeQuery = true)
	public Author searchByID(@Param("id") Long id);
	
	@Modifying
	@Query(value = "INSERT INTO Author (first_name, last_name, gender, age, country, rating) VALUES (:first_name, :last_name, :gender, :age, :country, :rating)", nativeQuery = true)
	@Transactional
	public void addAuthor(@Param("first_name") String firstName, @Param("last_name") String lastName, @Param("gender") String gender, @Param("age") int age, @Param("country") String country, @Param("rating") String rating);
	
	@Modifying
	@Query(value = "UPDATE Author SET first_name = :first_name, last_name = :last_name, gender = :gender, age = :age, country = :country, rating = :rating WHERE id = :id", nativeQuery = true)
	@Transactional
	public void updateAuthor(@Param("first_name") String firstName, @Param("last_name") String lastName, @Param("gender") String gender, @Param("age") int age, @Param("country") String country, @Param("rating") String rating, @Param("id") Long id);
	
	@Modifying
	@Query(value = "DELETE FROM Author WHERE id = :id", nativeQuery = true)
	@Transactional
	public void deleteAuthor(@Param("id") Long id);
	
	//- Author
//	*Author ID
//	* First Name
//	* Last Name
//	* Gender
//	* Age
//	* Country
//	* Rating
}