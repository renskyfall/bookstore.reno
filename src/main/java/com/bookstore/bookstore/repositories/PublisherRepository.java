package com.bookstore.bookstore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.bookstore.model.Paper;
import com.bookstore.bookstore.model.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {
	
	@Query(value = "select * from Publisher", nativeQuery = true)
	public List<Publisher> findAllActiveUsers();
	
	@Query(value = "select * from Publisher where id = :id", nativeQuery = true)
	public Publisher searchByID(@Param("id") Long id);
	
	@Modifying
	@Query(value = "INSERT INTO Publisher (company_name, pub_country, paper_id) VALUES (:company_name, :pub_country, :paper_id)", nativeQuery = true)
	@Transactional
	public void addPaper(@Param("company_name") String name, @Param("pub_country") String country, @Param("paper_id") Paper paper);
	
	@Modifying
	@Query(value = "UPDATE Publisher SET company_name = :company_name, pub_country = :pub_country, paper_id = :paper_id WHERE id = :id", nativeQuery = true)
	@Transactional
	public void updatePaper(@Param("company_name") String name, @Param("pub_country") String country, @Param("paper_id") Paper paper, @Param("id") Long id);
	
	@Modifying
	@Query(value = "DELETE FROM Publisher WHERE id = :id", nativeQuery = true)
	@Transactional
	public void deletePaper(@Param("id") Long id);
	
//	PUBLISHER
//	*Publisher ID
//	* Company Name
//	* Country
//	* Paper
}